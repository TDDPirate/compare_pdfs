#!/bin/bash
if [ "$1" == "-h" ] || [ "$1" == "--help" ] ; then
    echo "Usage: $0 file1.pdf file2.pdf"
    echo "Output: number of differing pixels"
    echo 'Result code (available from echo $?):'
    echo "  0 if the input files are pixel-identical"
    echo "  1 if there are any differing pixels"
    exit 0
fi
if [ "$#" -ne 2 ] ; then
    echo "Usage: $0 file1.pdf file2.pdf" >&2
    exit 1
fi
for inpfile in $1 $2 ; do
    if [ -z ${inpfile} ] || [ ! -e ${inpfile} ] ; then
	echo "${inpfile} not found" >&2
	exit 1
    elif [ ${inpfile: -4} != ".pdf" ] ; then
	echo "${inpfile} is not a PDF file" >&2
	exit 1
    fi
done

TMPDIR=$(mktemp -d)
PNG1=${TMPDIR}/png1.png
PNG2=${TMPDIR}/png2.png
PNGXOR=${TMPDIR}/png-xor.png
PNGXORTXT=${TMPDIR}/png-xor.txt

# Source: https://www.imagemagick.org/discourse-server/viewtopic.php?t=33090
convert -colorspace sRGB $1 -background white -alpha remove ${PNG1}
convert -colorspace sRGB $2 -background white -alpha remove ${PNG2}

# Source: https://stackoverflow.com/questions/8504882/searching-for-a-way-to-do-bitwise-xor-on-images
convert ${PNG1} ${PNG2} -fx "(((255*u)&(255*(1-v)))|((255*(1-u))&(255*v)))/255" ${PNGXOR}

convert ${PNGXOR} ${PNGXORTXT}
DIFFERING_PIXEL_COUNT=$(tail -n +2 ${PNGXORTXT} | fgrep -v "(0,0,0)  #000000  gray(0)" | wc -l)

rm -r $TMPDIR >&2
echo $DIFFERING_PIXEL_COUNT
if [ "$DIFFERING_PIXEL_COUNT" != 0 ] ; then
    exit 1
else
    exit 0
fi
# End of compare_pdfs.sh
