Usage: compare_pdfs.sh file1.pdf file2.pdf

Output: number of differing pixels

Result code (available from 'echo $?'):
  0 if the input files are pixel-identical
  1 if there are any differing pixels
  
NOTE: the script currently works only for single-page PDF files.